using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllyButtonsController : Effect
{
    [field: SerializeField]
    private Transform position { get; set; }

    [field: SerializeField]
    private Unit Pesant { get; set; }

    private Unit CreatedUnit { get; set; }
    public void CreatePesant() 
    {
        if (IsReady)
        {
            ResetData();
            StartReloading();
            CreatedUnit = Instantiate(Pesant, position);
        }
    }

    private void ResetData() 
    {
        IsReady = false;
        AlreadyReloaded = 0;
    }
}
