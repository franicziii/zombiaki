using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BazookaGroundBlast : BlastController
{
    protected override void Start()
    {
        StartCoroutine(DestroyInSeconds(true));
    }
}
