using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BazookaRocket : Bullet
{

    [field: SerializeField]
    protected BlastController BazookaBlast { get; set; }

    [field: SerializeField]
    protected BlastController GroundBazookaBlast { get; set; }

    private BlastController NewBazookBlast { get; set; }
    public override void DestroyBullet(bool ZombieHited = true)
    {
        if (!IsDestroyed)
        {
            Destroy(Collider);
            if (ZombieHited)
            {
                NewBazookBlast = Instantiate(BazookaBlast);
                NewBazookBlast.transform.position = this.transform.position;
            }
            else
            {
                NewBazookBlast = Instantiate(GroundBazookaBlast);
                NewBazookBlast.transform.position = this.transform.position;
            }
            
            Destroy(gameObject);
        }
    }
}
