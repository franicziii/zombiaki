using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Vector3 MousePosition { get; set; }
    private Vector3 MousePositioner { get; set; }

    protected bool IsDestroyed = false;

    [field: SerializeField]
    public Rigidbody2D RigidBody;

    [field: SerializeField]
    protected Collider2D Collider;

    [field: SerializeField]
    public Animator Animator { get; set; }

    [field: SerializeField]
    protected int MuzzleVelocity { get; set; }

    [field: SerializeField]
    public int Damage { get; set; }

    [field: SerializeField]
    public AttackType BulletAttackType { get; set; }

    public bool Hited { get; set; } = false;

    private const int DESTROY_SECONDS = 5;

    protected virtual void Start()
    {
        DestroyObjectAfterSecond();
    }
    protected virtual void Update() 
    {
    }
    protected void DestroyObjectAfterSecond()
    {
        Destroy(gameObject, DESTROY_SECONDS);
    }

    public virtual void DestroyBullet(bool ZombieHited = true)
    {
    }
    private void RotateToVelocity(Vector2 MousePostioner)
    {
        RigidBody.transform.rotation = Quaternion.identity;
        float angle = Vector2.Angle(new Vector2(-1, 0), MousePostioner);

        if(MousePositioner.y>=0)
            RigidBody.transform.Rotate(new Vector3(0,0, -angle));
        else
            RigidBody.transform.Rotate(new Vector3(0, 0, angle));
    }

    public void AddForceInMouseDirection(float Deviation = 0)
    {
        MousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MousePositioner = MousePosition - RigidBody.transform.position;
        Vector2 ForceToAdd = new Vector2(MousePositioner.x, MousePositioner.y).normalized * MuzzleVelocity;
        Vector2 PerpendicularForce = Vector2.Perpendicular(ForceToAdd).normalized;
        RigidBody.AddForce(ForceToAdd);
        RigidBody.AddForce(PerpendicularForce * Deviation);
        RotateToVelocity(MousePositioner);
    }

    protected virtual void BulletSpecialAction()
    {
    }

    public void OnTriggerEnter2D(Collider2D collision) 
    {
        
        if (collision.tag != "Ground")
            return;
        DestroyBullet(false);
    }
}