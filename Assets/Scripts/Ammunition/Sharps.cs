using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sharps : Bullet
{
    public override void DestroyBullet(bool ZombieHited = true)
    {
        if (!IsDestroyed)
        {
            Destroy(Collider);
            RigidBody.velocity = RigidBody.velocity / 14;
            if (ZombieHited)
            {
                Animator.SetTrigger("Destroyer");
                Destroy(gameObject, 0.25f);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
