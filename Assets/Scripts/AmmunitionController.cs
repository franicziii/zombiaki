using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AmmunitionController : MonoBehaviour
{
    [field: SerializeField]
    public _7_62Bullet _7_62Bullet { get; set; }
    [field: SerializeField]
    public Sharps Sharps { get; set; }
    [field: SerializeField]
    public _12_7Bullet _12_7Bullet { get; set; }
    [field: SerializeField]
    public _9mmBullet _9mmBullet { get; set; }

    [field: SerializeField]
    public BazookaRocket BazookaRocket { get; set; }

    [field: SerializeField]
    public Text CurrenAmmunitionText;

    [field: SerializeField]
    public Slider Slider { get; set; }

    [field: SerializeField]
    public UnityEvent<Weapon> OnAmmunitionChange { get; set; }

    [field: SerializeField]
    public UnityEvent<float,float> OnReloadTimerTick { get; set; }


    public void DisplayCurrentAmmunition(Weapon weapon)
    {
        if (weapon.GetType() == typeof(Shotgun))
            CurrenAmmunitionText.text = string.Format("{0}/{1}", (weapon.CurrentAmmo / weapon.OneShotAmmo).ToString(), (weapon.StartAmmo / weapon.OneShotAmmo).ToString());
        else
            CurrenAmmunitionText.text = string.Format("{0}/{1}", weapon.CurrentAmmo.ToString(), weapon.StartAmmo.ToString());
    }

    public void DisplayCurrentReloadedTime(float ReloadTime,float AlreadyReloadedTime) 
    {
        Slider.maxValue = ReloadTime;
        Slider.value = AlreadyReloadedTime;
    }



}
