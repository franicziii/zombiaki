using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlastController : MonoBehaviour
{
    [field: SerializeField]
    public int Damage { get; set; }

    [field: SerializeField]
    public AttackType BulletAttackType { get; set; }

    [field: SerializeField]
    public float Duration { get; set; }

    [field: SerializeField]
    public Collider2D Collider { get; set; }

    [field: SerializeField]
    public Animator Animator { get; set; }

    protected virtual void Start()
    {
        StartCoroutine(DestroyInSeconds());
    }

    protected IEnumerator DestroyInSeconds(bool LeaveTrace = false)
    {
        yield return new WaitForSeconds(Duration);
        Animator.SetTrigger("End");
        Destroy(Collider);
        if (!LeaveTrace)
        {
            yield return new WaitForSeconds(Duration);
            Destroy(gameObject);
        }
    }
}
