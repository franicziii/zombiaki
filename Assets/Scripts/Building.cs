using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : Healthy
{
    protected override void Start()
    {
        HealthPoints = MaxHealthPoints;
        OnHealthChange.Invoke(HealthPoints, MaxHealthPoints);
        base.Start();
    }
    protected override void Update()
    {
        base.Update();
    }

}
