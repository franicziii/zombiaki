using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCreator : MonoBehaviour
{
    Bullet bullet { get; set; }
    Weapon Weapon { get; set; }
    private bool IsReloading { get; set; }

    [field: SerializeField]
    HeroController hero { get; set; }

    [field: SerializeField]
    WeaponController WeaponController { get; set; }
    private KeyCode CurrenWeapon { get; set; }
    void Start()
    {
            InitiateValues();
    }

    private void Update()
    {
        if (Weapon != null && !hero.IsDead)
        {
            ShootAndReloadIfAble();
            ChangeWeapon();
        }
        if (Weapon != null && hero.IsDead) 
        {
            HideWeapon();
        }
    }

    private void HideWeapon()
    {
        Weapon.SpriteRenderer.enabled = false;
        Weapon.ammunitionController.Slider.value = 0;
        Weapon.ammunitionController.CurrenAmmunitionText.text = string.Empty;
    }

    private void ShootAndReloadIfAble()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            StartCoroutine(Weapon.Shoot(Weapon.TopOfGun));
        }
        if (Weapon.CurrentAmmo <= 0)
        {
            Weapon.Reload();
        }
    }

    private void InitiateValues()
    {
        if (hero.weaponArsenal[0] != null)
        {
            IsReloading = false;
            CurrenWeapon = KeyCode.Q;
            Weapon = hero.weaponArsenal[0];
            Weapon.SpriteRenderer.enabled = true;
            Weapon.ammunitionController.OnAmmunitionChange.Invoke(Weapon);
        }
        else if (hero.weaponArsenal[1] != null)
        {
            IsReloading = false;
            CurrenWeapon = KeyCode.W;
            Weapon = hero.weaponArsenal[1];
            Weapon.SpriteRenderer.enabled = true;
            Weapon.ammunitionController.OnAmmunitionChange.Invoke(Weapon);
        }
        else if (hero.weaponArsenal[2] != null)
        {
            IsReloading = false;
            CurrenWeapon = KeyCode.E;
            Weapon = hero.weaponArsenal[2];
            Weapon.SpriteRenderer.enabled = true;
            Weapon.ammunitionController.OnAmmunitionChange.Invoke(Weapon);
        }
        else if (hero.weaponArsenal[3] != null)
        {
            IsReloading = false;
            CurrenWeapon = KeyCode.R;
            Weapon = hero.weaponArsenal[3];
            Weapon.SpriteRenderer.enabled = true;
            Weapon.ammunitionController.OnAmmunitionChange.Invoke(Weapon);
        }
        else if (hero.weaponArsenal[4] != null)
        {
            IsReloading = false;
            CurrenWeapon = KeyCode.T;
            Weapon = hero.weaponArsenal[4];
            Weapon.SpriteRenderer.enabled = true;
            Weapon.ammunitionController.OnAmmunitionChange.Invoke(Weapon);
        }

    }

    protected void ChangeWeapon()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            PrepareWeapon(0);
        }
        else if (Input.GetKey(KeyCode.W))
        {
            PrepareWeapon(1);
        }

        else if (Input.GetKey(KeyCode.E))
        {
            PrepareWeapon(2);   
        }
        else if (Input.GetKey(KeyCode.R))
        {
            PrepareWeapon(3);
        }
        else if (Input.GetKey(KeyCode.T))
        {
            PrepareWeapon(4);
        }
    }

    private void PrepareWeapon(int number)
    {
        if (hero.weaponArsenal[number] != null)
        {
            Weapon.IsAnotherWeaponChosen = true;
            Weapon.SpriteRenderer.enabled = false;
            Weapon = hero.weaponArsenal[number];
            Weapon.SpriteRenderer.enabled = true;
            Weapon.IsAnotherWeaponChosen = false;

            Weapon.ammunitionController.OnAmmunitionChange.Invoke(Weapon);
            Weapon.ammunitionController.OnReloadTimerTick.Invoke(Weapon.ReloadTime, Weapon.AlreadyReloadedTime == 0 ? Weapon.ReloadTime : Weapon.AlreadyReloadedTime);
        }
    }
}
