using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Castle : Building
{
    [field: SerializeField]
    protected GameObject DefeatPanel { get; set; }
   
    public bool IsMouseOver {get;set;}

    protected override void Start()
    {
        IsCastle = true;
        GameStatus.IsCastleAlive = true;
        GameStatus.EarnedMoney = 0;
        base.Start();
    }

    protected override void Update()
    {
        if (IsDestroyed()) 
        {
            Defeat();
        }
        base.Update();
    }

    public void OnMouseEnter()
    {
        IsMouseOver = true;
    }
    public void OnMouseExit()
    {
        IsMouseOver = false;
    }

    private bool IsDestroyed() 
    {
        return HealthPoints <= 0;
    }
    private void Defeat() 
    {
        GameStatus.ExtraMoneyForLevel = 0;
        DefeatPanel.SetActive(true);
    }
}
