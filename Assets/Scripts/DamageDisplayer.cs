using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageDisplayer : MonoBehaviour
{
    [field: SerializeField]
    public Text Text { get; set; }
    [field: SerializeField]
    public Rigidbody2D Rigidbody { get; set; }

    private const int DESTROY_SECONDS = 2;
    protected virtual void Start()
    {
        DestroyObjectAfterSecond();
    }
    protected void DestroyObjectAfterSecond()
    {
        Destroy(gameObject, DESTROY_SECONDS);
    }
}
