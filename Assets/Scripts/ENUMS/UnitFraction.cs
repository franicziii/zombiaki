using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UnitFraction 
{
    ENEMY,
    ALLY,
    NEUTRAL
}
