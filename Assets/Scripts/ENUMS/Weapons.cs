using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Weapons
{
    PISTOL,
    AK47,
    SHOTGUN,
    M60,
    BAZOOKA
}

