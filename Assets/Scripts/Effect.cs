using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Effect : MonoBehaviour
{
    [field: SerializeField]
    public int Id { get; set; }

    [field: SerializeField]
    protected float Cooldown { get; set; }

    [field: SerializeField]
    public int Price { get; set; }


    [field: SerializeField]
    private Slider Slider { get; set; }
    [field: SerializeField]
    public Animator Animator { get; set; }

    [field: SerializeField]
    public UnityEvent<float, float> OnReloadTimerTick { get; set; }

    [field: SerializeField]
    public HeroController Hero { get; set; }

    [field: SerializeField]
    public bool IsAlly { get; set; }

    protected float AlreadyReloaded { get; set; }

    protected bool IsReady { get; set; }


    protected virtual void Start()
    {
        AlreadyReloaded = 0;
        IsReady = false;
        if (HaveThisEffect() || HaveThisAlly())
            StartCoroutine(ReloadSystem());
        else if(Slider != null)
            Slider.value = Slider.maxValue;
    }
    public bool HaveThisEffect() 
    {
        return Hero.specials[Id - 1] == 1 && !IsAlly;
    }

    public bool HaveThisAlly()
    {
        return Hero.soldiers[Id - 1] == 1 && IsAlly;
    }

    public void DisplayCurrentReloadedTime(float ReloadTime, float AlreadyReloadedTime)
    {
        Slider.maxValue = ReloadTime;
        Slider.value = ReloadTime - AlreadyReloadedTime;
    }
    public void StartReloading() 
    {
        if(HaveThisEffect() || HaveThisAlly())
            StartCoroutine(ReloadSystem());
    }

    public IEnumerator ReloadSystem()
    {
        for (float i = 0; i < Cooldown; i += 0.1f)
        {
            AlreadyReloaded += 0.1f;
            OnReloadTimerTick.Invoke(Cooldown, AlreadyReloaded);
            yield return new WaitForSeconds(0.1f);
        }
        IsReady = true;
    }
}