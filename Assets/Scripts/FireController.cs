using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireController : MonoBehaviour
{
    [field: SerializeField]
    public int Damage { get; set; }

    [field: SerializeField]
    public AttackType BulletAttackType { get; set; }

    [field: SerializeField]
    public float Duration { get; set; }

    [field: SerializeField]
    public Collider2D Collider { get; set; }

    protected void Start()
    {
        StartCoroutine(DestroyInSeconds());
    }

    protected IEnumerator DestroyInSeconds()
    {
        yield return new WaitForSeconds(Duration);
        Destroy(Collider);
        yield return new WaitForSeconds(0.01f);
        Destroy(gameObject);
    }
}
