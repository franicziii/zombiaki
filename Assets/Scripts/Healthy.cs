using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Healthy : MonoBehaviour
{
    [field: SerializeField]
    protected Rigidbody2D RigidBody { get; set; }
    [field: SerializeField]
    public SpriteRenderer SpriteRenderer { get; set; }
    [field: SerializeField]
    protected Collider2D Collider { get; set; }
    [field: SerializeField]
    public Animator Animator { get; set; }

    [field: SerializeField]
    public int HealthPoints { get; set; }
    [field: SerializeField]
    public int MaxHealthPoints { get; set; }
    [field: SerializeField]
    protected int Armor { get; set; }
    [field: SerializeField]
    public UnityEvent<int, int> OnHealthChange { get; set; }
   
    [field: SerializeField]
    public UnitFraction Fraction { get; set; }

    [field: SerializeField]
    public Canvas Canvas { get; set; }
    protected HeroController Hero { get; set; }

    [field: SerializeField]
    protected int Prize { get; set; }

    protected bool IsAlive = true;
    protected bool IsCastle = false;
    protected bool IsCastleAlieve = true;
    [field: SerializeField]
    public DamageDisplayer DamageDisplayer { get; set; }
    private DamageDisplayer DamageDisplayerInstance { get; set; }

    private int DamageToGet { get; set; }
    private List<int> DamageToDisplay { get; set; }

    protected virtual void Start()
    {
        Hero = GameObject.Find("Hero").GetComponent<HeroController>();
        DamageToDisplay = new List<int>();
    }

    protected virtual void Update()
    {
        if (IsAlive)
        {
            TryDie();
            if (DamageToDisplay.Count>0)
                StartCoroutine(TryToDisplayDamage());
        }
    }
    public void AddHealth(int value)
    {
        HealthPoints += value;
        OnHealthChange.Invoke(HealthPoints, MaxHealthPoints);
    }
    public void GetDamage(int value, AttackType attackType) 
    {
        DamageToGet = attackType == AttackType.PHYSICAL ? value - Armor : value;
        if (DamageToGet <= 0)
            DamageToGet = 1;
        DamageToDisplay.Add(DamageToGet);
        AddHealth(-DamageToGet);
    }
    protected void TryDie()
    {
        if (HealthPoints <= 0)
        {
            IsAlive = false;
            if (IsCastle)
            {
                GameStatus.IsCastleAlive = false;
            }
            RigidBody.bodyType = RigidbodyType2D.Kinematic;

            if (!IsCastle)
            {
                MoveDeadBody();
            }
            else 
            {
                Hero.SpriteRenderer.enabled = false;
                Hero.IsDead = true;
            }
            if (Fraction.Equals(UnitFraction.ENEMY) && GameStatus.IsCastleAlive) 
            {
                Hero.money += Prize;
                GameStatus.EarnedMoney += Prize;
            }

            Destroy(Collider);
            Canvas.enabled = false;
            if (!IsCastle)
            {
                SpriteRenderer.color = Color.white;
            }
            Animator.SetTrigger("Death");
        }
    }

    protected void JustDie() 
    {
        Destroy(RigidBody.gameObject);
        if (IsCastle)
        {
            IsCastleAlieve = false;
            Defeat();
            Hero.SavePlayer();
        }
    }

    protected void MoveDeadBody() 
    {
        if (Fraction == UnitFraction.ENEMY)
        {
            RigidBody.velocity = new Vector2(0, 0);
        }
        else if (Fraction == UnitFraction.ALLY)
        {
            RigidBody.velocity = new Vector2(3, 0);
        }
        else if (Fraction == UnitFraction.NEUTRAL)
        {
            RigidBody.velocity = new Vector2(0, 0);
        }
    }

    private void CreateDamageDisplayer(int value) 
    {
        if (value < 0)
            value = -value;
        DamageDisplayerInstance = Instantiate(DamageDisplayer, transform);
        DamageDisplayerInstance.transform.localScale = 1/transform.lossyScale.magnitude * DamageDisplayer.transform.localScale*2;
        DamageDisplayerInstance.Rigidbody.AddForce(new Vector3(6000,3000, 0));
        DamageDisplayerInstance.Text.text = value.ToString();
    }

    private IEnumerator TryToDisplayDamage() 
    {
        yield return new WaitForSeconds(0.1f);
        List<int> CurrentDamageToDisplay = new List<int>(DamageToDisplay);
        DamageToDisplay.Clear();
        foreach (int damage in CurrentDamageToDisplay) 
        {
            CreateDamageDisplayer(damage);
            yield return new WaitForSeconds(0.1f);
        }
    }

    private void Defeat()
    {
        GameStatus.ExtraMoneyForLevel = 0;
        Time.timeScale = 0.1f;
    }
}
