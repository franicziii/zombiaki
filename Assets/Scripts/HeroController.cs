using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HeroController : MonoBehaviour
{

    private void Start()
    {
        IsDead = false;
        if (GameStatus.CurrentSlot != 0)
        {
            LoadPlayer(GameStatus.CurrentSlot);
        }
        NumerateWeapons();
        LoadArsenal();
        if (tag != "MenuItem")
        {
            LoadUpgrades();
        }
    }
    [field: SerializeField]
    public int currentLevel { get; set; }
    [field: SerializeField]
    public int money { get; set; }
    [field: SerializeField]
    public int stars { get; set; }
    [field: SerializeField]
    public int[] weapons { get; set; }
    [field: SerializeField]
    public int[] arsenal { get; set; }
    [field: SerializeField]
    public Weapon[] weaponArsenal { get; set; }
    [field: SerializeField]
    public Weapon[] allWeapons { get; set; }
    [field: SerializeField]
    public int[] ammunition { get; set; }
    [field: SerializeField]
    public int[] soldiers { get; set; }
    [field: SerializeField]
    public int[] upgrades { get; set; }
    [field: SerializeField]
    public int[] specials { get; set; }
    [field: SerializeField]
    public int[] completedLevels { get; set; }
    [field: SerializeField]
    MoneyAndStarsController MoneyAndStarsController { get; set; }

    [field: SerializeField]
    UpgradeController UpgradeController { get; set; }

    [field: SerializeField]
    public SpriteRenderer SpriteRenderer { get; set; }

    public bool IsDead { get; set; }

    private Dictionary<int, Weapon> NumeratedWeapons = new Dictionary<int, Weapon>();

    private const int NUMBEROFWEAPONS = 5;

    public void SavePlayer()
    {
        SaveSystem.SavePlayer(this, GameStatus.CurrentSlot.ToString());
    }

    public void LoadPlayer(int slot)
    {
        Player player = SaveSystem.LoadPlayer(slot.ToString());
        money = player.money;
        stars = player.stars;
        weapons = player.weapons;
        ammunition = player.ammunition;
        soldiers = player.soldiers;
        specials = player.specials;
        arsenal = player.arsenal;
        upgrades = player.upgrades;
        completedLevels = player.completedLevels;
        currentLevel = player.currentLevel;
    }

    public void SetStartEquipment()
    {
        money = 7500;
        for (int i = 0; i < weapons.Length; i++)
        {
            weapons[i] = 0;
        }
        for (int i = 0; i < ammunition.Length; i++)
        {
            ammunition[i] = 0;
        }
        for (int i = 0; i < soldiers.Length; i++)
        {
            soldiers[i] = 0;
        }
        for (int i = 0; i < specials.Length; i++)
        {
            specials[i] = 0;
        }
        for (int i = 0; i < arsenal.Length; i++)
        {
            arsenal[i] = 0;
        }
        for (int i = 0; i < upgrades.Length; i++)
        {
            upgrades[i] = 0;
        }
        for (int i = 0; i < completedLevels.Length; i++)
        {
            completedLevels[i] = 0;
        }
    }

    public void NumerateWeapons()
    {
        for (int NumberOfWeapons = 1; NumberOfWeapons <= NUMBEROFWEAPONS; NumberOfWeapons++)
        {
            NumeratedWeapons.Add(NumberOfWeapons, allWeapons[NumberOfWeapons - 1]);
        }
    }
    private void LoadUpgrades()
    {
        UpgradeController.LoadUpgrades();
    }
    public void LoadArsenal()
    {
        int currentNumber = 0;
        foreach (int weaponNumber in arsenal)
        {
            try
            {
                if (HaveWeapon(weaponNumber))
                {
                    weaponArsenal[currentNumber] = NumeratedWeapons[weaponNumber];
                    weaponArsenal[currentNumber].IsInArsenal = true;
                }
                else
                {
                    weaponArsenal[currentNumber] = null;
                }
            }
            catch (KeyNotFoundException) { }
            currentNumber++;
        }
    }

    public void BuyWeapon(Weapon weapon)
    {
        switch (weapon.name)
        {
            case "AK47":
                if (weapon.Price <= money && weapons[(int)Weapons.AK47] != 1)
                {
                    weapons[(int)Weapons.AK47] = 1;
                    money -= weapon.Price;
                }
                break;
            case "Shotgun":
                if (weapon.Price <= money && weapons[(int)Weapons.SHOTGUN] != 1)
                {
                    weapons[(int)Weapons.SHOTGUN] = 1;
                    money -= weapon.Price;
                }
                break;
            case "M60":
                if (weapon.Price <= money && weapons[(int)Weapons.M60] != 1)
                {
                    weapons[(int)Weapons.M60] = 1;
                    money -= weapon.Price;
                }
                break;
            case "Pistol":
                if (weapon.Price <= money && weapons[(int)Weapons.PISTOL] != 1)
                {
                    weapons[(int)Weapons.PISTOL] = 1;
                    money -= weapon.Price;
                }
                break;
            case "Bazooka":
                if (weapon.Price <= money && weapons[(int)Weapons.BAZOOKA] != 1)
                {
                    weapons[(int)Weapons.BAZOOKA] = 1;
                    money -= weapon.Price;
                }
                break;
        }
        MoneyAndStarsController.OnMoneyChange.Invoke();
        SavePlayer();
    }

    public void BuyEffect(Effect effect)
    {
        switch (effect.name)
        {
            case "Rocket":
                if (effect.Price <= money && specials[(int)Effects.ROCKETSTRIKE] != 1)
                {
                    specials[(int)Effects.ROCKETSTRIKE] = 1;
                    money -= effect.Price;
                }
                break;
            case "Fire":
                if (effect.Price <= money && specials[(int)Effects.NAPALMSTRIKE] != 1)
                {
                    specials[(int)Effects.NAPALMSTRIKE] = 1;
                    money -= effect.Price;
                }
                break;
            case "Nuclear Bomb":
                if (effect.Price <= money && specials[(int)Effects.NUCLEARSTRIKE] != 1)
                {
                    specials[(int)Effects.NUCLEARSTRIKE] = 1;
                    money -= effect.Price;
                }
                break;
        }
        MoneyAndStarsController.OnMoneyChange.Invoke();
        SavePlayer();
    }

    public void BuyInfected(AllyController Ally)
    {
        switch (Ally.name)
        {
            case "Pesant":
                if (Ally.Price <= money && soldiers[(int)Allies.PESANT] != 1)
                {
                    soldiers[(int)Allies.PESANT] = 1;
                    money -= Ally.Price;
                }
                break;
        }
        MoneyAndStarsController.OnMoneyChange.Invoke();
        SavePlayer();
    }

    public void BuyUpgrade(int Price, string Name)
    {
        switch (Name)
        {
            case "AK47_1":
                if (Price <= money && upgrades[(int)Upgrades.AK47_1] != 1)
                {
                    upgrades[(int)Upgrades.AK47_1] = 1;
                    money -= Price;
                }
                break;

        }
        MoneyAndStarsController.OnMoneyChange.Invoke();
        SavePlayer();
    }

    public bool HaveWeapon(int index)
    {
        if (index != 0)
        {
            return weapons[index - 1] != 0;
        }
        else
            return false;
    }
}
