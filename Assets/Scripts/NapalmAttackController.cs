using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NapalmAttackController : Effect
{
    [field: SerializeField]
    private Transform position { get; set; }

    [field: SerializeField]
    private NapalmBombController NapalmBomb { get; set; }

    private NapalmBombController NapalmBombInstance { get; set; }

    private int BombAmount = 6;

    private Vector3 StartPosition { get; set; }

    private bool IsDropingBombs = false;

    protected override void Start()
    {
        StartPosition = position.position;
        base.Start();
    }
    public void DropNapalm()
    {
        if (!IsDropingBombs && IsReady && HaveThisEffect())
        {
            StartCoroutine(DropNapalmWork());
        }
    }

    public IEnumerator DropNapalmWork()
    {
        ResetCooldown();
        StartReloading();
        IsDropingBombs = true;
        for (int i = 0; i < BombAmount; i++)
        {
            NapalmBombInstance = Instantiate(NapalmBomb, position);
            NapalmBombInstance.transform.SetParent(null);
            yield return new WaitForSeconds(0.3f);
            position.transform.position -= position.transform.right.normalized * 3.5f;
        }
        ResetData();
    }
    public void ResetData() 
    {
        position.position = StartPosition;
        IsDropingBombs = false;
        IsReady = false;
    }

    public void ResetCooldown() 
    {
        AlreadyReloaded = 0;
        OnReloadTimerTick.Invoke(Cooldown, AlreadyReloaded);
    }
}
