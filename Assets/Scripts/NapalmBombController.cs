using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NapalmBombController : MonoBehaviour
{
    [field: SerializeField]
    public FireController Fire { get; set; }
    protected FireController FireInstance { get; set; }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        CheckCollision();
    }
    private void CheckCollision()
    {
        FireInstance = Instantiate(Fire, transform);
        FireInstance.transform.SetParent(null);

        Destroy(gameObject);
    }
}
