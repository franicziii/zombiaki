using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoShotAreaController : MonoBehaviour
{
    public bool IsMouseOver { get; set; }

    [field: SerializeField]
    private Castle Castle;

    public void OnMouseEnter()
    {
        Castle.IsMouseOver = true;

    }
    public void OnMouseExit()
    {
        Castle.IsMouseOver = false;
    }

    public void UnblockShoting() 
    {
        Castle.IsMouseOver = false;
    }
}
