using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NuclearAttackController : Effect
{
    [field: SerializeField]
    private Transform position { get; set; }

    [field: SerializeField]
    private RocketController Rocket { get; set; }

    private RocketController CreatedRocket { get; set; }

    private int RocketAmount = 1;

    private Vector3 StartPosition { get; set; }

    protected override void Start()
    {
        StartPosition = position.position;
        base.Start();
    }
    public void DropRockets()
    {
        if (IsReady && HaveThisEffect())
        {
            DropRocketWork();
        }
    }

    public void DropRocketWork()
    {
        ResetCooldown();
        StartReloading();
        CreatedRocket = Instantiate(Rocket, position);
        CreatedRocket.transform.SetParent(null);
        ResetData();
    }

    public void ResetData()
    {
        IsReady = false;
    }


    public void ResetCooldown()
    {
        AlreadyReloaded = 0;
        OnReloadTimerTick.Invoke(Cooldown, AlreadyReloaded);
    }
}
