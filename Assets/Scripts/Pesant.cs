using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pesant : AllyController
{
    protected override IEnumerator UseSkill()
    {
        IsUsingSkill = true;
        Armor += 3;
        Mana = 0;
        //  SpriteRenderer.color = Color.blue;
        SpriteRenderer.color = Color.black;
        SpriteRenderer.sortingOrder = 23;
        yield return new WaitForSeconds(SkillDuration);
    //    SpriteRenderer.color = Color.white;
        Armor -= 3;
        IsUsingSkill = false;
    }
}
