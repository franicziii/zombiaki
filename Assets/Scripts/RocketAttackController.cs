using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketAttackController : Effect
{

    [field: SerializeField]
    private Transform position { get; set; }

    [field: SerializeField]
    private RocketController Rocket { get; set; }

    private RocketController CreatedRocket { get; set; }

    private int RocketAmount = 6;

    private Vector3 StartPosition { get; set; }

    private bool IsDropingRockets = false;

    protected override void Start()
    {
        StartPosition = position.position;
        base.Start();
    }
    public void DropRockets()
    {
        if (!IsDropingRockets && IsReady && HaveThisEffect())
        {
            StartCoroutine(DropRocketWork());
        }
    }

    public IEnumerator DropRocketWork()
    {
        ResetCooldown();
        StartReloading();
        IsDropingRockets = true;
        for (int i = 0; i < RocketAmount; i++)
        {
            CreatedRocket = Instantiate(Rocket, position);
            CreatedRocket.transform.SetParent(null);
            yield return new WaitForSeconds(0.3f);
            position.transform.position -= position.transform.right.normalized * 5.5f;
        }
        ResetData();
    }

    public void ResetData()
    {
        position.position = StartPosition;
        IsDropingRockets = false;
        IsReady = false;
    }


    public void ResetCooldown()
    {
        AlreadyReloaded = 0;
        OnReloadTimerTick.Invoke(Cooldown, AlreadyReloaded);
    }
}
