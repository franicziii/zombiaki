using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketController : MonoBehaviour
{
    [field: SerializeField]
    public BlastController Blast { get; set; }
    protected BlastController BlastInstance { get; set; }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        CheckCollision();
    }
    private void CheckCollision() 
    {
        if (name == "NuclearRocket(Clone)")
        {
            Castle castle = FindObjectOfType<Castle>();
            if (castle != null)
            {
                castle.HealthPoints -= Convert.ToInt32(castle.MaxHealthPoints * 0.4f);
                castle.OnHealthChange.Invoke(castle.HealthPoints, castle.MaxHealthPoints);
            }
        }
        BlastInstance = Instantiate(Blast, transform);
        BlastInstance.transform.Rotate(new Vector3(0,0,-90));
        BlastInstance.transform.SetParent(null);

        Destroy(gameObject);
    }
}
