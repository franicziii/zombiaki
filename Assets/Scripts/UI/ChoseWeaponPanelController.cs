using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoseWeaponPanelController : MonoBehaviour
{
    [field: SerializeField]
    DropAndDragWeapon[] weapons { get; set; }
    private void OnEnable()
    {
        foreach (DropAndDragWeapon weapon in weapons)
        {
            weapon.SetColor();
            weapon.PlaceWeaponsOnPositions();
        }
    }
}
