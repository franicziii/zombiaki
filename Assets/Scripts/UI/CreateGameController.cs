using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class CreateGameController : MonoBehaviour
{
    [field: SerializeField]
    protected GameObject MenuPanel { get; set; }
    [field: SerializeField]
    protected GameObject MapPanel { get; set; }
    [field: SerializeField]
    protected HeroController Hero { get; set; }
    [field: SerializeField]
    protected MoneyAndStarsController MoneyAndStarsController { get; set; }

    public void StartNewGame(int slot)
    {
        GameStatus.CurrentSlot = slot;
        Hero.SetStartEquipment();
        MapPanel.SetActive(true);
        MenuPanel.SetActive(false);
        SavePlayer();
        MoneyAndStarsController.DisplayCurrentStarsAndMoney();
        Hero.LoadArsenal();
    }

    public void ContinueGame(int slot) 
    {
        GameStatus.CurrentSlot = slot;
        string path = Application.persistentDataPath + string.Format("/player.dex{0}", slot);
        if (File.Exists(path))
        {
            Hero.LoadPlayer(slot);
            MapPanel.SetActive(true);
            MenuPanel.SetActive(false);
            SavePlayer();
            MoneyAndStarsController.DisplayCurrentStarsAndMoney();
            Hero.LoadArsenal();
        }
    }

    public void BackToMainMenu() 
    {
        GameStatus.CurrentSlot = 0;
        MoneyAndStarsController.DisplayNothing();
        MapPanel.SetActive(false);
        MenuPanel.SetActive(true);
    }

    public void SavePlayer() 
    {
        Hero.SavePlayer();
    }

    public void Exit() 
    {
        Application.Quit();
    }
}
