using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropAndDragWeapon : MonoBehaviour
{
    [field: SerializeField]
    protected int Index { get; set; }

    [field: SerializeField]
    protected SlotController[] Slots { get; set; }

    [field: SerializeField]
    protected HeroController Hero { get; set; }

    [field: SerializeField]
    public Vector3 startPosition;

    [field: SerializeField]
    public SlotController CurrentSlot { get; set; }
    [field: SerializeField]
    public SlotController OldSlot { get; set; }
    private Vector3 _dragOffset;
    private Camera _cam;
    public Collider2D Collider;
    private SpriteRenderer SpriteRenderer;


    private void Awake()
    {
        Collider = GetComponent<Collider2D>();
        _cam = Camera.main;
    }
    private void OnMouseEnter()
    {
        if (CurrentSlot != null)
        {
            CurrentSlot.IsActive = true;
        }
    }

    private void OnMouseExit()
    {
        if (CurrentSlot != null)
        {
            CurrentSlot.IsActive = false;
        }
    }

    public void SetColor() 
    {
        SpriteRenderer = GetComponent<SpriteRenderer>();
        Color newColor = SpriteRenderer.color;
        if (!Hero.HaveWeapon(Index))
        {
            newColor.a = 0.5f;
            SpriteRenderer.color = newColor;
        }
        else
        {
            newColor.a = 1f;
            SpriteRenderer.color = newColor;
        }
    }

    private void OnMouseDown()
    {
        if (Hero.HaveWeapon(Index))
        {
            Collider.enabled = false;
            
        }
        if (CurrentSlot != null)
        {
            Hero.arsenal[CurrentSlot.Index - 1] = 0;
            CurrentSlot.BusyWith = null;
            CurrentSlot = null;
        }
        _dragOffset = transform.position - GetMousePos();
    }
    private void OnMouseUp()
    {
        if (Hero.HaveWeapon(Index))
        {
            foreach (SlotController slot in Slots)
            {
                if (slot.IsActive)
                {
                    if (slot.BusyWith != null)
                    {
                        if (OldSlot != null)
                        {
                            slot.BusyWith.CurrentSlot = OldSlot;
                            slot.BusyWith.OldSlot = OldSlot;
                            slot.BusyWith.transform.localPosition = OldSlot.transform.localPosition;
                            slot.BusyWith.transform.localPosition = new Vector3(slot.BusyWith.transform.localPosition.x, slot.BusyWith.transform.localPosition.y, -1);
                            Hero.arsenal[OldSlot.Index - 1] = slot.BusyWith.Index;
                            OldSlot.BusyWith = slot.BusyWith;
                        }
                        else
                        {
                            slot.BusyWith.transform.localPosition = slot.BusyWith.startPosition;
                            slot.BusyWith.CurrentSlot = null;
                            slot.BusyWith.OldSlot = null;
                        }
                    }
                    transform.position = slot.transform.position;
                    transform.position = new Vector3(transform.position.x, transform.position.y, 1);
                    slot.BusyWith = this;
                    Collider.enabled = true; 
                    CurrentSlot = slot;
                    OldSlot = CurrentSlot;
                    Hero.arsenal[slot.Index - 1] = Index;
                    Hero.SavePlayer();
                    return;
                }
            }
            transform.localPosition = startPosition;
            OldSlot = null;
            Collider.enabled = true;
        }
    }
    private void OnMouseDrag()
    {
        if (Hero.HaveWeapon(Index))
        {
            transform.position = GetMousePos() + _dragOffset;
        }
    }

    private Vector3 GetMousePos()
    {
        var mousePos = _cam.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;
        return mousePos;
    }

    public void PlaceWeaponsOnPositions()
    {
        transform.localPosition = startPosition;
        Hero.LoadArsenal();
        for (int i = 0; i < 5; i++)
        {
            if (Hero.weaponArsenal[i] != null && Hero.weaponArsenal[i].Index == Index && Hero.HaveWeapon(Index))
            {
                transform.position = Slots[i].transform.position;
                transform.position = new Vector3(transform.position.x, transform.position.y, -1);
                Slots[i].BusyWith = this;
                CurrentSlot = Slots[i];
                OldSlot = CurrentSlot;
            }
        }
    }
}
