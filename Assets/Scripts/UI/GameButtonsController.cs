using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameButtonsController : MonoBehaviour
{

    [field: SerializeField]
    protected ZombieCreator ZombieCreator { get; set; }
    [field: SerializeField]
    protected HeroController Hero { get; set; }
    public void ChangeSceneTo(string SceneName)
    {
        ZombieCreator.Reset();
        Time.timeScale = 1;
        GameStatus.IsGameStarted = true;
        if(SceneName.Equals("MainMenuScene"))
            SceneManager.LoadScene(SceneName);
        else
        SceneManager.LoadScene(SceneName + Hero.currentLevel.ToString());
        
    }
    public void PauseGame() 
    {
        Time.timeScale = 0;
    }
    public void ResumeGame()
    { 
        Time.timeScale = 1f; 
    }

}
