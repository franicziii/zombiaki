using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameStatus
{
    public static bool IsGameStarted { get; set; } = false;
    public static int CurrentSlot { get; set; } = 0;
    public static int EarnedMoney { get; set; } = 0;
    public static int ExtraMoneyForLevel { get; set; } = 0;

    public static bool IsCastleAlive = true;
}
