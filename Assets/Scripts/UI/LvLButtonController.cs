using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LvLButtonController : MonoBehaviour
{
    [field: SerializeField]
    protected int LevelNumber { get; set; }
    [field: SerializeField]
    protected HeroController Hero { get; set; }
    private void OnEnable()
    {
       GetComponent<Button>().interactable = Hero.completedLevels[LevelNumber - 2] > 0;
    }
}
