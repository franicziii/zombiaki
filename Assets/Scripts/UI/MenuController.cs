using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    [field: SerializeField]
    protected GameObject MapPanel { get; set; }
    [field: SerializeField]
    protected GameObject MenuPanel { get; set; }
    [field: SerializeField]
    protected MoneyAndStarsController MoneyAndStarsController { get; set; }
    void Start()
    {
        if (GameStatus.IsGameStarted)
        {
            MenuPanel.SetActive(false);
            MapPanel.SetActive(true);
            MoneyAndStarsController.DisplayCurrentStarsAndMoney();
        }
    }
}
