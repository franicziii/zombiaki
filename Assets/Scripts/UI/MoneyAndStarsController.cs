using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MoneyAndStarsController : MonoBehaviour
{
    [field: SerializeField]
    public UnityEvent OnMoneyChange { get; set; }

    [field: SerializeField]
    Text CurrenMoneyText;

    [field: SerializeField]
    Text CurrenStarsText;

    [field: SerializeField]
    HeroController Hero;
    public void DisplayCurrentStarsAndMoney()
    {
        CurrenMoneyText.text = string.Format("$: {0}", (Hero.money).ToString());
        CurrenStarsText.text = string.Format("Stars: {0}", (Hero.stars).ToString());
    }

    public void DisplayNothing() 
    {
        CurrenMoneyText.text = string.Empty;
        CurrenStarsText.text = string.Empty;
    }
}
