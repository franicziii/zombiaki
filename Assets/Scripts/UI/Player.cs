using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Player
{
    public int money;
    public int stars;
    public int currentLevel;
    public int[] weapons;
    public int[] ammunition;
    public int[] soldiers;
    public int[] specials;
    public int[] arsenal;
    public int[] upgrades;
    public int[] completedLevels;

    public Player(HeroController hero) 
    {
        money = hero.money;
        stars = hero.stars;
        weapons = hero.weapons;
        ammunition = hero.ammunition;
        soldiers = hero.soldiers;
        specials = hero.specials;
        arsenal = hero.arsenal;
        upgrades = hero.upgrades;
        completedLevels = hero.completedLevels;
        currentLevel = hero.currentLevel;
    }
}
