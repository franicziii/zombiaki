using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void SavePlayer(HeroController hero, string slot)
    {
        BinaryFormatter formatter = new BinaryFormatter();

        string path = Application.persistentDataPath + string.Format("/player.dex{0}",slot);

        FileStream stream = new FileStream(path, FileMode.Create);

        Player player = new Player(hero);

        formatter.Serialize(stream, player);
        stream.Close();
    }

    public static Player LoadPlayer(string slot) 
    {

        string path = Application.persistentDataPath + string.Format("/player.dex{0}", slot);
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();

            FileStream stream = new FileStream(path, FileMode.Open);

            Player player = formatter.Deserialize(stream) as Player;

            stream.Close();

            return player;
        }
        else
        {
            return null;
        }
    }
}
