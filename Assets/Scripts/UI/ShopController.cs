using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopController : MonoBehaviour
{
    [field: SerializeField]
    protected GameObject ConfirmPanel { get; set; }

    [field: SerializeField]
    protected Text ConfirmPanelText { get; set; }

    [field: SerializeField]
    protected GameObject NoEnoughFoundsPanel { get; set; }

    [field: SerializeField]
    protected ShopPanelController ShopPanelController { get; set; }

    [field: SerializeField]
    protected ShopEffectsController ShopEffectsController { get; set; }

    [field: SerializeField]
    protected ShopInfectedController ShopInfectedController { get; set; }

    [field: SerializeField]
    protected HeroController Hero { get; set; }


    protected Weapon CurrentWeapon;
    protected Effect CurrentEffect;
    protected AllyController CurrentAlly;

    public void Buy() 
    {
        if (CurrentWeapon != null)
            BuyWeapon();
        if (CurrentEffect != null)
            BuyEffect();
        if (CurrentAlly != null)
            BuyAlly();
    }

    public void ConfirmWeapon(Weapon weapon) 
    {
        ConfirmPanel.SetActive(true);
        ConfirmPanelText.text = string.Format("Are you sure you want to buy {0} for {1}", weapon.name, weapon.Price.ToString());
        CurrentWeapon = weapon;
    }

    public void BuyWeapon() 
    {
        if (Hero.money >= CurrentWeapon.Price)
        {
            Hero.BuyWeapon(CurrentWeapon);
            ConfirmPanel.SetActive(false);
            ShopPanelController.DeactivateOrActivateWeaponButtons();
            CurrentWeapon.enabled = true;
        }
        else
            NoEnoughFoundsPanel.SetActive(true);
        CurrentWeapon = null;
    }

    public void ConfirmEffect(Effect effect)
    {
        ConfirmPanel.SetActive(true);
        ConfirmPanelText.text = string.Format("Are you sure you want to buy {0} for {1}", effect.name, effect.Price.ToString());
        CurrentEffect = effect;
    }

    public void BuyEffect()
    {
        if (Hero.money >= CurrentEffect.Price)
        {
            Hero.BuyEffect(CurrentEffect);
            ConfirmPanel.SetActive(false);
            ShopEffectsController.DeactivateOrActivateWeaponButtons();
            CurrentEffect.Animator.enabled = true;
        }
        else
            NoEnoughFoundsPanel.SetActive(true);
        CurrentEffect = null;
    }

    public void ConfirmAlly(AllyController Ally)
    {
        ConfirmPanel.SetActive(true);
        ConfirmPanelText.text = string.Format("Are you sure you want to buy {0} for {1}", Ally.name, Ally.Price.ToString());
        CurrentAlly = Ally;
    }

    public void BuyAlly()
    {
        if (Hero.money >= CurrentAlly.Price)
        {
            Hero.BuyInfected(CurrentAlly);
            ConfirmPanel.SetActive(false);
            ShopInfectedController.DeactivateOrActivateWeaponButtons();
        }
        else
            NoEnoughFoundsPanel.SetActive(true);
        CurrentAlly = null;
    }
}
