using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ShopEffectsController : MonoBehaviour
{
    [field: SerializeField]
    protected GameObject RocketLounchButton { get; set; }
    [field: SerializeField]
    protected Text RocketLounchButtonText { get; set; }

    [field: SerializeField]
    protected Effect RocketLounch { get; set; }

    [field: SerializeField]
    protected GameObject NapalmButton { get; set; }
    [field: SerializeField]
    protected Text NapalmButtonText { get; set; }
    [field: SerializeField]
    protected Effect Napalm { get; set; }
    [field: SerializeField]
    protected GameObject NuclearBombButton { get; set; }
    [field: SerializeField]
    protected Text NuclearBombButtonText { get; set; }
    [field: SerializeField]
    protected Effect NuclearBomb { get; set; }

    [field: SerializeField]
    protected HeroController Hero { get; set; }


    private void OnEnable()
    {
        DeactivateOrActivateWeaponButtons();
    }

    public void DeactivateOrActivateWeaponButtons()
    {
        RocketLounch.Animator.enabled = Convert.ToBoolean(Hero.specials[(int)Effects.ROCKETSTRIKE]);
        RocketLounchButton.SetActive(!RocketLounch.Animator.enabled);
        RocketLounchButtonText.text = String.Format("Buy {0} for {1}$", RocketLounch.name, RocketLounch.Price.ToString());

        Napalm.Animator.enabled = Convert.ToBoolean(Hero.specials[(int)Effects.NAPALMSTRIKE]);
        NapalmButton.SetActive(!Napalm.Animator.enabled);
        NapalmButtonText.text = String.Format("Buy {0} for {1}$", Napalm.name, Napalm.Price.ToString());

        NuclearBomb.Animator.enabled = Convert.ToBoolean(Hero.specials[(int)Effects.NUCLEARSTRIKE]);
        NuclearBombButton.SetActive(!NuclearBomb.Animator.enabled);
        NuclearBombButtonText.text = String.Format("Buy {0} for {1}$", NuclearBomb.name, NuclearBomb.Price.ToString());


    }
}
