using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ShopInfectedController : MonoBehaviour
{
    [field: SerializeField]
    protected GameObject PesantButton { get; set; }
    [field: SerializeField]
    protected Text PesantButtonText { get; set; }
    [field: SerializeField]
    protected AllyController Pesant { get; set; }


    [field: SerializeField]
    protected HeroController Hero { get; set; }


    private void OnEnable()
    {
        DeactivateOrActivateWeaponButtons();
    }

    public void DeactivateOrActivateWeaponButtons()
    {
        Pesant.Animator.enabled = Convert.ToBoolean(Hero.soldiers[(int)Allies.PESANT]);
        PesantButton.SetActive(!Pesant.Animator.enabled);
        PesantButtonText.text = String.Format("Buy {0} for {1}$", Pesant.name, Pesant.Price.ToString());
    }
}
