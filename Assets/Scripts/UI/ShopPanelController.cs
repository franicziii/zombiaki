using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopPanelController : MonoBehaviour
{
    [field: SerializeField]
    protected GameObject AK47Button { get; set; }
    [field: SerializeField]
    protected Text AK47ButtonText { get; set; }

    [field: SerializeField]
    protected GameObject AK47_1Button { get; set; }
    [field: SerializeField]
    protected Text AK47_1ButtonText { get; set; }

    [field: SerializeField]
    protected Weapon AK47 { get; set; }

    [field: SerializeField]
    protected GameObject PistolButton { get; set; }
    [field: SerializeField]
    protected Text PistolButtonText { get; set; }
    [field: SerializeField]
    protected Weapon Pistol { get; set; }

    [field: SerializeField]
    protected GameObject ShotgutButton { get; set; }
    [field: SerializeField]
    protected Text ShotgunButtonText { get; set; }
    [field: SerializeField]
    protected Weapon Shotgun { get; set; }
    [field: SerializeField]
    protected GameObject M60Button { get; set; }
    [field: SerializeField]
    protected Text M60ButtonText { get; set; }
    [field: SerializeField]
    protected Weapon M60 { get; set; }

    [field: SerializeField]
    protected GameObject BazookaButton { get; set; }
    [field: SerializeField]
    protected Text BazookaButtonText { get; set; }
    [field: SerializeField]
    protected Weapon Bazooka { get; set; }

    [field: SerializeField]
    protected HeroController Hero { get; set; }


    private void OnEnable()
    {
        DeactivateOrActivateWeaponButtons();
        DeactivateOrActivateUpgradeButtons();
        ResetWeaponRotation();
    }

    public void DeactivateOrActivateWeaponButtons() 
    {
        AK47.enabled = Convert.ToBoolean(Hero.weapons[(int)Weapons.AK47]);
        AK47Button.SetActive(!AK47.enabled);
        AK47ButtonText.text = String.Format("Buy {0} for {1}$",AK47.name, AK47.Price.ToString());

        Shotgun.enabled = Convert.ToBoolean(Hero.weapons[(int)Weapons.SHOTGUN]);
        ShotgutButton.SetActive(!Shotgun.enabled);
        ShotgunButtonText.text = String.Format("Buy {0} for {1}$", Shotgun.name, Shotgun.Price.ToString());

        M60.enabled = Convert.ToBoolean(Hero.weapons[(int)Weapons.M60]);
        M60Button.SetActive(!M60.enabled);
        M60ButtonText.text = String.Format("Buy {0} for {1}$", M60.name, M60.Price.ToString());

        Pistol.enabled = Convert.ToBoolean(Hero.weapons[(int)Weapons.PISTOL]);
        PistolButton.SetActive(!Pistol.enabled);
        PistolButtonText.text = String.Format("Buy {0} for {1}$", Pistol.name, Pistol.Price.ToString());

        Bazooka.enabled = Convert.ToBoolean(Hero.weapons[(int)Weapons.BAZOOKA]);
        BazookaButton.SetActive(!Bazooka.enabled);
        BazookaButtonText.text = String.Format("Buy {0} for {1}$", Bazooka.name, Bazooka.Price.ToString());
    }

    public void DeactivateOrActivateUpgradeButtons()
    {
        AK47_1Button.SetActive(!Convert.ToBoolean(Hero.upgrades[(int)Upgrades.AK47_1]));
        AK47_1ButtonText.text = String.Format("Upgrade for 100$");
    }

    public void ResetWeaponRotation() 
    {
        AK47.transform.rotation = Quaternion.identity;
        Shotgun.transform.rotation = Quaternion.identity;
        M60.transform.rotation = Quaternion.identity;
        Pistol.transform.rotation = Quaternion.identity;
        Bazooka.transform.rotation = Quaternion.identity;
    }
}
