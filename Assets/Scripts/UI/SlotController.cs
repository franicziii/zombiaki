using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SlotController : MonoBehaviour
{

    [field: SerializeField]
    public int Index { get; set; }

    [field: SerializeField]
    public DropAndDragWeapon BusyWith { get; set; }

    [field: SerializeField]
    protected HeroController Hero { get; set; }

    [field: SerializeField]
    protected Collider2D Collider { get; set; }
    [field: SerializeField]
    public bool IsActive { get; set; }

    private void Start()
    {
        IsActive = false;
    }

    private void OnMouseEnter()
    {
        IsActive = true;
    }

    private void OnMouseExit()
    {
        IsActive = false;
    }
}
