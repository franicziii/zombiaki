using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartButtonController : MonoBehaviour
{
    [field: SerializeField]
    protected HeroController Hero { get; set; }
    public void ChangeSceneToGameScene(string SceneName)
    {
        Hero.SavePlayer();
        SceneManager.LoadScene(SceneName+Hero.currentLevel.ToString());
    }
    public void SetCurrentLevel(int LevelNumber) 
    {
        Hero.currentLevel = LevelNumber;
        Hero.SavePlayer();
    }
}
