using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinAndLosePanel : MonoBehaviour
{
    [field: SerializeField]
    public Text Text { get; set; }
    [Multiline]
    public string MoneyInfo;
    private void OnEnable()
    {
        MoneyInfo = string.Format("Money earned:\nZombies: {0}\nFirst time Level success: {1}", GameStatus.EarnedMoney.ToString(), GameStatus.ExtraMoneyForLevel.ToString());
        Text.text = MoneyInfo;
    }
}
