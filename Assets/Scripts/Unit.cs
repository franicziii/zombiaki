using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Unit : Healthy
{
    protected bool IsAttacking { get; set; } = false;
    [field: SerializeField]
    protected AttackType AttackType { get; set; }

    private bool IsRightDirection { get; set; }
    //Statistics
    [field: Space]
    [field: SerializeField]
    protected int Mana { get; set; }
    [field: SerializeField]
    protected int MaxMana { get; set; }
    [field: SerializeField]
    protected int ManaIncreaseAmount { get; set; }
    [field: SerializeField]
    protected float ManaIncreaseTime { get; set; }
    [field: SerializeField]
    protected int Speed { get; set; }
    [field: SerializeField]
    protected float AttackPerSecond { get; set; }
    [field: SerializeField]
    protected int Damage { get; set; }

    [field: SerializeField]
    protected float SkillDuration { get; set; }

    [field: SerializeField]
    protected UnityEvent<int, int> OnManaChange { get; set; }

    [field: SerializeField]
    public int DefaultLayer { get; set; }

    [field: SerializeField]
    public int AttackLayer { get; set; }
    [field: SerializeField]
    public bool IsUsingSkill { get; set; }
    [field: SerializeField]
    public Unit FightingWith { get; set; }
    public bool IsFighting { get; set; }
    private bool CollisionIgnored { get; set; } = false;

    private List<BlastController> BlastCollisions = new List<BlastController>();

    private int BlastDamageToGet { get; set; }

    private int FireDamageToGet { get; set; }
    private bool IsGettingBlastDamage { get; set; } = false;
    public bool IsBurning { get; private set; } = false;

    private Dictionary<BlastController, Coroutine> CurrentBlastIn = new Dictionary<BlastController, Coroutine>();

    private Coroutine BurningCorutine { get; set; }

    protected override void Start()
    {
        IsUsingSkill = false;
        HealthPoints = MaxHealthPoints;
        OnHealthChange.Invoke(HealthPoints, MaxHealthPoints);
        OnManaChange.Invoke(Mana, MaxMana);
        StartCoroutine(AddManaInTime());
        StartCoroutine(BlastCollision());
        StartCoroutine(FireCollision());
        base.Start();
        Move();
    }

    protected override void Update()
    {
        if (IsAlive)
        {
            Move();
            if (Mana >= MaxMana && !IsUsingSkill)
            {
                StartCoroutine(UseSkill());
            }
        }
        CheckIsFightingWithIsAliveAndDeleteItIfItsTrue();
        base.Update();
    }
    protected virtual void BulletCollisionSpecialAction()
    { }
    protected virtual IEnumerator UseSkill()
    {
        Mana = 0;
        yield return null;
    }
    protected void Move()
    {
        if (Fraction == UnitFraction.ENEMY && !IsFighting)
            RigidBody.velocity = new Vector2(Speed, RigidBody.velocity.y);
        else if (Fraction == UnitFraction.ALLY && !IsFighting)
            RigidBody.velocity = new Vector2(-Speed, RigidBody.velocity.y);
        else if (IsFighting)
            RigidBody.velocity = new Vector2(0, 0);
    }


    public void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.layer != 0)
            CheckCollision(collision.collider);
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (gameObject.layer != 9 && !CollisionIgnored)
        {
            IsFighting = false;
            gameObject.layer = DefaultLayer;
        }
        CollisionIgnored = false;
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer != 0 && collision.gameObject.tag != "Blast" && collision.gameObject.tag != "Fire")
            CheckCollision(collision);
        else if (collision.gameObject.tag == "Blast")
        {
            BlastDamageToGet += collision.gameObject.GetComponent<BlastController>().Damage;
        }
    }
    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Blast")
        {
            StopBlastDamage(collision.gameObject.GetComponent<BlastController>());
        }
        else if (collision.gameObject.tag == "Fire")
        {
            FireDamageToGet = 0;
        }
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Fire") 
        {
                FireDamageToGet = collision.gameObject.GetComponent<FireController>().Damage > FireDamageToGet ? collision.gameObject.GetComponent<FireController>().Damage: FireDamageToGet;
        }
    }

    private void StopBlastDamage(BlastController blast)
    {
        BlastDamageToGet -= blast.Damage;
    }

    private void CheckCollision(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            BulletCollision(collision.gameObject.GetComponent<Bullet>());
        }
        else if (collision.gameObject.tag == "Healthy")
        {
            Unit TargetUnit = collision.gameObject.GetComponent<Unit>();
            Healthy target = collision.gameObject.GetComponent<Healthy>();
            if (target.gameObject.layer != 9 && !(TargetUnit.FightingWith == null && FightingWith == null))
            {
                if (TargetUnit.FightingWith != this)
                {
                    CollisionIgnored = true;
                    TargetUnit.CollisionIgnored = true;
                    Physics2D.IgnoreCollision(collision, Collider, true);
                }
            }
            if (!CollisionIgnored)
            {
                if (target.gameObject.layer != 9 && this.gameObject.layer != 9)
                {
                    FightingWith = TargetUnit;
                    TargetUnit.FightingWith = this;
                }
                if (Fraction != target.Fraction)
                {
                    if (target.gameObject.layer != 9)
                    {
                        IsFighting = true;
                        gameObject.layer = AttackLayer;
                    }
                }
                if (Fraction != target.Fraction && !IsAttacking)
                {
                    StartCoroutine(Attack(target));
                }
            }
        }
    }
    protected IEnumerator Attack(Healthy target)
    {
        IsAttacking = true;
        target.GetDamage(Damage, AttackType);
        yield return new WaitForSeconds(1f / AttackPerSecond);
        IsAttacking = false;
    }
    private void BulletCollision(Bullet bullet)
    {
        if (!bullet.Hited)
        {
            bullet.Hited = true;
            GetDamage(bullet.Damage, bullet.BulletAttackType);
            AddMana(bullet.Damage);
            BulletCollisionSpecialAction();
            bullet.DestroyBullet();
        }
    }

    private IEnumerator BlastCollision()
    {
        while (true)
        {
            
            if (BlastDamageToGet > 0)
            {
                GetDamage(BlastDamageToGet, AttackType.BLAST);
            }
            yield return new WaitForSeconds(0.1f);
        }
    }

    private IEnumerator FireCollision()
    {
        while (true)
        {
            if (FireDamageToGet > 0)
            {
                GetDamage(FireDamageToGet, AttackType.FIRE);
            }
            yield return new WaitForSeconds(0.3f);
        }
    }

    protected void AddMana(int ManaToAdd)
    {
        if (!IsUsingSkill)
            Mana += ManaToAdd;
        OnManaChange.Invoke(Mana, MaxMana);
    }
    protected IEnumerator AddManaInTime()
    {
        while (IsAlive)
        {
            yield return new WaitForSeconds(ManaIncreaseTime);
            AddMana(ManaIncreaseAmount);
        }
    }
    protected void CheckIsFightingWithIsAliveAndDeleteItIfItsTrue()
    {
        if (FightingWith != null && !FightingWith.IsAlive)
            FightingWith = null;
    }


}
