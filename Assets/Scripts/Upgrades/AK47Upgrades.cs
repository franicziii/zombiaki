using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AK47Upgrades
{
    public static void IncreaseAK47DMG(HeroController hero) 
    {
        hero.allWeapons[(int)Weapons.AK47].ammunitionController._7_62Bullet.Damage = 25;
    }
}
