using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class UpgradeController : ShopController
{
    protected Dictionary<string, int> allUpgrades = new Dictionary<string, int>();
    private int CurrentPrice { get; set; }
    private string CurrentName { get; set; }
    public void LoadUpgrades()
    {
        if (Hero.upgrades[0] == 1)
            AK47Upgrades.IncreaseAK47DMG(Hero);
    }

    public void ConfirmUpgrade(int DictionaryPosition)
    {
        FillUpgrades();
        ConfirmPanel.SetActive(true);
        ConfirmPanelText.text = string.Format("Are you sure you want to buy this upgrade for {0}", allUpgrades.ElementAt(DictionaryPosition - 1).Value);
        CurrentPrice = allUpgrades.ElementAt(DictionaryPosition - 1).Value;
        CurrentName = allUpgrades.ElementAt(DictionaryPosition - 1).Key;
    }
    public void BuyUpgrade()
    {
        if (Hero.money >= CurrentPrice)
        {
            Hero.BuyUpgrade(CurrentPrice, CurrentName);
            ConfirmPanel.SetActive(false);
            ShopPanelController.DeactivateOrActivateUpgradeButtons();
        }
        else
            NoEnoughFoundsPanel.SetActive(true);
    }

    private void FillUpgrades()
    {
        allUpgrades.Add("AK47_1", 100);
    }
}
