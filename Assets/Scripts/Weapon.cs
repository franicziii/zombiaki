using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public abstract class Weapon : MonoBehaviour
{
    [field: SerializeField]
    public int Index { get; set; }

    [field: SerializeField]
    public Transform TopOfGun { get; set; }

    [field: SerializeField]
    public Transform SpawnPoint { get; set; }

    [field: SerializeField]
    public SpriteRenderer SpriteRenderer { get; set; }
    [field: SerializeField]
    protected int BasicDamage { get; set; }

    [field: SerializeField]
    public int StartAmmo { get; set; }
    [field: SerializeField]
    public int CurrentAmmo { get; set; }
    [field: SerializeField]
    public float BulletDeviation { get; set; }
    [field: SerializeField]
    public float ReloadTime { get; set; }
    [field: SerializeField]
    public float ShootTime { get; set; }
    [field: SerializeField]
    public int OneShotAmmo;

    [field: SerializeField]

    public AmmunitionController ammunitionController { get; set; }
    
    [field: SerializeField]

    public bool IsAnotherWeaponChosen;

    private bool IsReloading { get; set; }
    [field: SerializeField]
    public float AlreadyReloadedTime { get; set; }
    [field: SerializeField]
    protected Castle castle { get; set; }
    [field: SerializeField]
    public int Price { get; set; }

    private Vector3 Direction { get; set; }
    private float Angle { get; set; }
    [field: SerializeField]
    public bool IsInArsenal { get; set; } = false;


    public abstract IEnumerator Shoot(Transform transform);
    public abstract void InitiateValues();

    public virtual void Awake()
    {
        CurrentAmmo = StartAmmo;
        if (ammunitionController != null)
            ammunitionController.OnAmmunitionChange.Invoke(this);
    }


    public virtual void Update()
    {
        RotateToMouse();
    }
    public float GenerateRandomFloat(float Deviation) 
    {
        return Random.Range(-Deviation, Deviation);
    }
    public void Reload() 
    {
        StartCoroutine(ReloadSystem());
        
    }

    public void StopReloading() 
    {
        StopCoroutine(ReloadSystem());
    }

    public IEnumerator ReloadSystem()
    {
        float CurrentReloadTime = ReloadTime - AlreadyReloadedTime;
        if (!IsReloading)
        {
            IsReloading = true;
            IsAnotherWeaponChosen = false;

            for (float i = 0; i < CurrentReloadTime; i += 0.1f)
            {
                if (!IsAnotherWeaponChosen)
                {
                    AlreadyReloadedTime += 0.1f;
                    ammunitionController.OnReloadTimerTick.Invoke(ReloadTime, AlreadyReloadedTime == 0 ? ReloadTime : AlreadyReloadedTime);
                }
                yield return new WaitForSeconds(0.1f);
            }

            if (!IsAnotherWeaponChosen && AlreadyReloadedTime >= ReloadTime)
            {
                CurrentAmmo = StartAmmo;
                ammunitionController.OnAmmunitionChange.Invoke(this);
                AlreadyReloadedTime = 0;
            }
            
            IsAnotherWeaponChosen = false;
            IsReloading = false;
        }
    }

    public void RotateToMouse() 
    {
        Direction = transform.position - Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Angle = Mathf.Atan2(Direction.y, Direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(Angle, Vector3.forward);
    }

}
