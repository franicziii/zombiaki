using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    [field: SerializeField]
    public AK47 AK47 { get; set; }
    [field: SerializeField]
    public Shotgun Shotgun { get; set; }

    [field: SerializeField]
    public CKM CKM { get; set; }
}
