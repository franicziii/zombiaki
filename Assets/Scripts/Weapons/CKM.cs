using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CKM : Weapon
{
    Bullet bulletInstance { get; set; }
    public Bullet Bullet { get; set; }

    private int ammo;
    [field: SerializeField]
    private bool IsShooting = false;
    [field: SerializeField]
    private Transform Hero { get; set; }

    public override void Awake()
    {
        InitiateValues();
        base.Awake();
    }
    public override IEnumerator Shoot(Transform SpawnPoint)
    {
        ammo = CurrentAmmo;
        if (!IsShooting && CurrentAmmo > 0)
        {
            IsShooting = true;
            for (int i = 0; i < ammo; i++)
            {
                if (Input.GetKey(KeyCode.Mouse0))
                {
                    if (IsAnotherWeaponChosen || castle.IsMouseOver)
                    {
                        break;
                    }

                    bulletInstance = Instantiate(Bullet, SpawnPoint);
                    bulletInstance.transform.SetParent(Hero);
                    bulletInstance.AddForceInMouseDirection(GenerateRandomFloat(BulletDeviation));
                    ammo--;
                    CurrentAmmo = ammo;
                    ammunitionController.OnAmmunitionChange.Invoke(this);
                }
                yield return new WaitForSeconds(ShootTime);
            }
            IsShooting = false;
        }
        yield return null;
    }

    public override void InitiateValues()
    {
        if (ammunitionController != null)
        {
            Bullet = ammunitionController._12_7Bullet;
            Bullet.Damage = BasicDamage;
        }
        IsShooting = false;
    }
}
