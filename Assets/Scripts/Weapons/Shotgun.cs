using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Weapon
{
    Bullet bulletInstance { get; set; }
    public Bullet Bullet { get; set; }

    private int ammo;
    
    [field: SerializeField]
    private bool IsShooting = false;
    [field: SerializeField]
    private Transform Hero { get; set; }

    public override void Awake()
    {
        InitiateValues();
        base.Awake();
    }
    public override IEnumerator Shoot(Transform SpawnPoint)
    {
        ammo = CurrentAmmo;
        if (!IsShooting && CurrentAmmo>0 && !castle.IsMouseOver)
        {
            IsShooting = true;
            float startXposition = TopOfGun.transform.position.x;
            float startYposition = TopOfGun.transform.position.y;
            for (int i = 0; i < OneShotAmmo; i++)
            {
                bulletInstance = Instantiate(Bullet, SpawnPoint);
                bulletInstance.transform.SetParent(Hero);
                bulletInstance.AddForceInMouseDirection(GenerateRandomFloat(BulletDeviation));
                ammo--;
                CurrentAmmo = ammo;
                ammunitionController.OnAmmunitionChange.Invoke(this);
                TopOfGun.transform.position += TopOfGun.transform.right.normalized * 0.1f;
            }
            TopOfGun.transform.position = new Vector2(startXposition, startYposition);
            yield return new WaitForSeconds(ShootTime);
            IsShooting = false;
        }
        yield return null;
    }

    public override void InitiateValues()
    {
        if (ammunitionController != null)
        {
            
            Bullet = ammunitionController.Sharps;
            Bullet.Damage = BasicDamage;
        }
        IsShooting = false;
    }
}
