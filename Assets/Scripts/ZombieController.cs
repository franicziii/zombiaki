using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieController : MonoBehaviour
{
    [field: SerializeField]
    public FatZombie FatZombie { get; set; }
    [field: SerializeField]
    public NormalZombie NormalZombie { get; set; }
    [field: SerializeField]
    public FastZombie FastZombie { get; set; }
}
