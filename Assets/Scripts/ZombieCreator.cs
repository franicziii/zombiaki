using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieCreator : MonoBehaviour
{
    private bool IsSpawning = false;
    private bool IsCheckingWin = false;

    [field: SerializeField]
    protected HeroController Hero { get; set; }

    [field: SerializeField]
    private ZombieController ZombieController { get; set; }
    [field: SerializeField]
    protected GameObject SuccessPanel { get; set; }
    [field: SerializeField]
    private GameObject ZombieSpawnPlace { get; set; }
    private FatZombie newFatZombie { get; set; }
    private NormalZombie newNormalZombie { get; set; }
    private FastZombie newFastZombie { get; set; }
    private Unit newUnit { get; set; }

    private static int wafe = 1;

    private static int newUnitOrderInLayer = 1;

    void Start()
    {
        InitiateValues();
    }
    private void Update()
    {
        StartCoroutine(CheckWin());
        Spawn();
    }
    private void Spawn()
    {
        //Zombie/Time/Amount/CurrentWafe


        switch (Hero.currentLevel)
        {
            case 1:
                LVL1();
                break;
            case 2:
                LVL2();
                break;
            case 3:
                LVL3();
                break;
        }


    }

    private void LVL1()
    {
        GameStatus.ExtraMoneyForLevel = 150;
        StartCoroutine(SpawnZombie(newNormalZombie, 2f, 4, 1));
        StartCoroutine(SpawnZombie(newNormalZombie, 1.5f, 2, 2));
        StartCoroutine(SpawnZombie(newNormalZombie, 7f, 2, 3));
        StartCoroutine(SpawnZombie(newNormalZombie, 9f, 2, 4));
        StartCoroutine(SpawnZombie(newNormalZombie, 1.5f, 2, 5));
        StartCoroutine(SpawnZombie(newNormalZombie, 9f, 2, 6));
        StartCoroutine(SpawnZombie(newNormalZombie, 7f, 2, 7));
        StartCoroutine(SpawnZombie(newNormalZombie, 2f, 4, 8));
    }
    private void LVL2()
    {
        GameStatus.ExtraMoneyForLevel = 350;
        StartCoroutine(SpawnZombie(newNormalZombie, 1.5f, 4, 1));
        StartCoroutine(SpawnZombie(newFastZombie, 0.5f, 2, 2));
        StartCoroutine(SpawnZombie(newNormalZombie, 1.5f, 4, 3));
        StartCoroutine(SpawnZombie(newFastZombie, 0.5f, 3, 4));
        StartCoroutine(SpawnZombie(newNormalZombie, 1f, 6, 5));
        StartCoroutine(SpawnZombie(newFastZombie, 0.8f, 4, 6));
        StartCoroutine(SpawnZombie(newNormalZombie, 4f, 1, 7));
        StartCoroutine(SpawnZombie(newNormalZombie, 1f, 6, 8));
        StartCoroutine(SpawnZombie(newFastZombie, 0.8f, 4, 9));
        StartCoroutine(SpawnZombie(newNormalZombie, 1f, 3, 10));
        StartCoroutine(SpawnZombie(newNormalZombie, 4f, 1, 11));
        StartCoroutine(SpawnZombie(newFastZombie, 0.8f, 7, 12));
        StartCoroutine(SpawnZombie(newFastZombie, 12f, 1, 13));
        StartCoroutine(SpawnZombie(newNormalZombie, 0.8f, 3, 14));
        StartCoroutine(SpawnZombie(newFastZombie, 2.8f, 4, 15));
        StartCoroutine(SpawnZombie(newNormalZombie, 0.8f, 3, 16));
        StartCoroutine(SpawnZombie(newFastZombie, 3.8f, 4, 17));
        StartCoroutine(SpawnZombie(newNormalZombie, 0.8f, 3, 18));
        StartCoroutine(SpawnZombie(newFastZombie, 3.8f, 4, 19));
        StartCoroutine(SpawnZombie(newNormalZombie, 0.8f, 3, 20));
        StartCoroutine(SpawnZombie(newFastZombie, 0.8f, 5, 21));
        StartCoroutine(SpawnZombie(newNormalZombie, 0.8f, 9, 22));
        StartCoroutine(SpawnZombie(newFastZombie, 0.8f, 5, 23));
        StartCoroutine(SpawnZombie(newNormalZombie, 0.8f, 9, 24));
        StartCoroutine(SpawnZombie(newFastZombie, 0.8f, 6, 25));




    }
    private void LVL3()
    {
        GameStatus.ExtraMoneyForLevel = 13500;
        StartCoroutine(SpawnZombie(newNormalZombie, 1.5f, 4, 1));
        StartCoroutine(SpawnZombie(newFatZombie, 0.5f, 2, 2));
        StartCoroutine(SpawnZombie(newFastZombie, 0.8f, 6, 3));
        StartCoroutine(SpawnZombie(newFatZombie, 0.5f, 2, 4));
        StartCoroutine(SpawnZombie(newNormalZombie, 0.5f, 15, 5));
        StartCoroutine(SpawnZombie(newFatZombie, 0.7f, 4, 6));
        StartCoroutine(SpawnZombie(newFastZombie, 0.4f, 2, 7));
        StartCoroutine(SpawnZombie(newNormalZombie, 0.8f, 10, 8));
        StartCoroutine(SpawnZombie(newFatZombie, 1.7f, 4, 9));
        StartCoroutine(SpawnZombie(newFastZombie, 0.4f, 2, 10));
        StartCoroutine(SpawnZombie(newNormalZombie, 0.5f, 2, 11));
        StartCoroutine(SpawnZombie(newFatZombie, 1.7f, 5, 12));
        StartCoroutine(SpawnZombie(newNormalZombie, 0.8f, 18, 13));
        StartCoroutine(SpawnZombie(newFastZombie, 0.4f, 2, 14));
        StartCoroutine(SpawnZombie(newNormalZombie, 0.5f, 10, 15));
        StartCoroutine(SpawnZombie(newFastZombie, 0.4f, 4, 16));
        StartCoroutine(SpawnZombie(newFatZombie, 0.9f, 5, 17));
        StartCoroutine(SpawnZombie(newNormalZombie, 0.9f, 5, 18));
        StartCoroutine(SpawnZombie(newFatZombie, 0.5f, 2, 19));
        StartCoroutine(SpawnZombie(newNormalZombie, 0.5f, 10, 20));
        StartCoroutine(SpawnZombie(newFastZombie, 0.4f, 4, 21));
        StartCoroutine(SpawnZombie(newFatZombie, 0.9f, 5, 22));
        StartCoroutine(SpawnZombie(newNormalZombie, 0.8f, 10, 23));
        StartCoroutine(SpawnZombie(newNormalZombie, 15f, 1, 24));
        StartCoroutine(SpawnZombie(newFatZombie, 0.9f, 5, 25));
        StartCoroutine(SpawnZombie(newFastZombie, 0.8f, 5, 26));
        StartCoroutine(SpawnZombie(newFatZombie, 0.9f, 5, 27));
        StartCoroutine(SpawnZombie(newNormalZombie, 0.5f, 3, 28));
        StartCoroutine(SpawnZombie(newFastZombie, 0.8f, 5, 29));
        StartCoroutine(SpawnZombie(newFatZombie, 0.7f, 8, 30));
        StartCoroutine(SpawnZombie(newFastZombie, 0.4f, 3, 31));






    }

    private IEnumerator SpawnZombie(Unit zombie, float time, int amount, int currentWafe)
    {
        if (!IsSpawning && currentWafe == wafe)
        {
            IsSpawning = true;
            for (int i = 0; i < amount; i++)
            {
                yield return new WaitForSeconds(time);
                newUnit = Instantiate(zombie, ZombieSpawnPlace.transform);
                newUnitOrderInLayer++;
                newUnit.SpriteRenderer.sortingOrder = newUnitOrderInLayer;
            }
            wafe++;
            IsSpawning = false;
        }

    }

    private void InitiateValues()
    {
        IsSpawning = false;
        newFatZombie = ZombieController.FatZombie;
        newNormalZombie = ZombieController.NormalZombie;
        newFastZombie = ZombieController.FastZombie;
    }

    public void Reset()
    {
        wafe = 1;
    }

    private IEnumerator CheckWin()
    {

        if (!IsCheckingWin)
        {
            IsCheckingWin = true;
            yield return new WaitForSeconds(3);
            if (FindObjectsOfType<Zombie>().Length <= 0 && !IsSpawning)
            {
                Time.timeScale = 0.1f;
                if (Hero.completedLevels[Hero.currentLevel - 1] == 1)
                {
                    GameStatus.ExtraMoneyForLevel = 0;
                }
                SuccessPanel.SetActive(true);
                Hero.money += GameStatus.ExtraMoneyForLevel;
                Hero.completedLevels[Hero.currentLevel - 1] = 1;
                GameStatus.ExtraMoneyForLevel = 0;
                Hero.SavePlayer();
            }
        }
        IsCheckingWin = false;
    }

}
