using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZombieEventController : MonoBehaviour
{
    [field: SerializeField]
    private Slider HealthSlider { get; set; }
    [field: SerializeField]
    private Slider ManaSlider { get; set; }
    public void SetLife(int currentLife, int maxLife) 
    {
        HealthSlider.maxValue = maxLife;
        HealthSlider.value = currentLife;
    }
    
    public void SetMana(int currentMana, int maxMana)
    {
        ManaSlider.maxValue = maxMana;
        ManaSlider.value = currentMana;
    }
}
