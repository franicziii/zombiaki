using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastZombie : Zombie
{
    protected override void BulletCollisionSpecialAction()
    {
    }

    protected override IEnumerator UseSkill()
    {
        yield return new WaitForSeconds(SkillDuration);

    }
}
