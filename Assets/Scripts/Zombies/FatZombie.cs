using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FatZombie : Zombie
{
    protected override void BulletCollisionSpecialAction()
    {
        if (!IsAttacking)
            Speed = 7;
    }

    protected override IEnumerator UseSkill()
    {
        IsUsingSkill = true;
        Speed += 1;
        Damage += 15;
        Mana = 0;
        SpriteRenderer.color = Color.red;
        yield return new WaitForSeconds(SkillDuration);
        SpriteRenderer.color = Color.white;
        Damage -= 15;
        Speed -= 1;
        IsUsingSkill = false;
    }
}
