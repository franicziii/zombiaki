using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalZombie : Zombie
{
    protected override IEnumerator UseSkill()
    {
        IsUsingSkill = true;
        Armor += 3;
        Mana = 0;
        SpriteRenderer.color = Color.blue;
        yield return new WaitForSeconds(SkillDuration);
        SpriteRenderer.color = Color.white;
        Armor -= 3;
        IsUsingSkill = false;
    }
}
